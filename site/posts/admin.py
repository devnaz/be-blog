from django.contrib import admin
from .models import Post

# Register your models here.

class PostAdmin(admin.ModelAdmin):
    list_display = ['title', 'timestamp']
    list_display_links = ['timestamp']
    # list_editable = ['title']   # can rename this field
    list_filter = ['update', 'timestamp']
    search_fields = ['title', 'content']


admin.site.register(Post, PostAdmin)

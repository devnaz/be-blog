from urllib.parse import quote_plus
from django.contrib import messages
from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.db.models import Q
from .forms import PostForm
from .models import Post

# Create your views here.
def post_create(request):
    # you can't get this page if you're not admin
    if not request.user.is_staff or not request.user.is_superuser:
        raise Http404()

    # even if user not registered we will raise an error
    if not request.user.is_authenticated():
        raise Http404()

    form = PostForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        instance = form.save(commit=False)
        # do something before save the form
        print(form.cleaned_data.get("title"))
        instance.user = request.user
        instance.save()
        # success mesage
        messages.success(request, "Was Created!", extra_tags='created')
        return HttpResponseRedirect(instance.get_absolute_url())

    # if request.method == "POST":
    #     title = request.POST.get("title")
    #     content = request.POST.get("content")
    #     Post.objects.create(title=title, content=content)
    return render(request, "post_form.html", {"form": form, "title": "Create new post here!"})


def post_detail(request, id=None):
    # instance = get_object_or_404(Post, id=24)
    try:
        instance = Post.objects.get(id=id)
    except:
        raise Http404()

    share_text = quote_plus(instance.content)

    context = {
        "obj": instance,
        "title": "Detail",
        "share_text": share_text
    }
    return render(request, "post_detail.html", context)


def post_list(request):
    queryset_list = Post.objects.all()  #.order_by("-timestamp")

    query = request.GET.get('q')
    if query:
        queryset_list = queryset_list.filter(Q(title__icontains=query) |
                                            Q(content__icontains=query)
                                            # Q(user__first__name__icontains=query)
                                        ) #.distinct()

    paginator = Paginator(queryset_list, 4) # Show 4 posts per page
    page = request.GET.get('p')

    try:
        queryset = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        queryset = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        queryset = paginator.page(paginator.num_pages)
    context = {
        "object_list": queryset,
        "title": "My posts"
    }
    # if request.user.is_authenticated():
    #     context = {
    #         "title": "My list"
    #     }
    # else:
    #     context = {
    #         "title": "List"
    #     }
    return render(request, "post_list.html", context)


def post_update(request, id=None):
    # you can't get this page if you're not admin
    if not request.user.is_staff or not request.user.is_superuser:
        raise Http404()
    post = get_object_or_404(Post, id=id)
    form = PostForm(request.POST or None, request.FILES or None, instance=post)
    if form.is_valid():
        form.save()
        # success message
        messages.success(request, "<a href='#'>Item</a> Was Changed!", extra_tags='my-class')
        return HttpResponseRedirect(post.get_absolute_url())

    context = {
        "form": form,
        "title": "Edit the post here!"
    }

    return render(request, "post_form.html", context)


def post_delete(request, id=None):
    if not request.user.is_staff or not request.user.is_superuser:
        raise Http404()
    post = get_object_or_404(Post, id=id)
    post.delete()
    messages.success(request, "Was Deleted")
    return redirect("posts:list")

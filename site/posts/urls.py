from django.conf.urls import url
# from . import views
from .views import (
    post_list,
    post_create,
    post_detail,
    post_update,
    post_delete,
)

urlpatterns = [
    url(r'^$', post_list, name='list'),
    url(r'^create/$', post_create, name='create'),
    url(r'^(?P<id>\d+)/$', post_detail, name='detail'),
    url(r'^(?P<id>\d+)/edit/$', post_update, name='update'),
    url(r'^(?P<id>\d+)/delete/$', post_delete, name='del'),


    # url(r'^$', views.post_list),
    # url(r'^create/$', views.post_create),
    # url(r'^detail/$', views.post_detail),
    # url(r'^update/$', views.post_update),
    # url(r'^delete/$', views.post_delete),

    # url(r'^$', "posts.views.post_home"),
    # url(r'^$', "<app_name>.views.<function_name>"),

]

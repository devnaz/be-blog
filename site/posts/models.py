# coding = utf-8
from django.conf import settings

from django.db import models
from django.core.urlresolvers import reverse
from django.db.models.signals import pre_save

from django.utils import timezone
from django.utils.text import slugify


# Create your models here.
# MVC - Model View Controller

# Post.objects.all()
class PostManager(models.Manager):
    def my_method(self, *args, **kwargs):  # we overwrite this method all / in views it'll be like this Post.objects.my_method()
        # Post.objects.all() --> super(PostManager, self).all()
        return super(PostManager, self).filter(draft=False).filter(publish__lte=timezone.now())


def upload_locations(instance, filename):
    # filebase, extension = filename.split(".")
    return "%s" %(filename)

class Post(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1)  # blank=True, null=True

    title = models.CharField(max_length=120)
    slug = models.SlugField(unique=True)
    image = models.ImageField(upload_to=upload_locations, null=True, blank=True, height_field="height_field", width_field="width_field")  # save width and height our image
    height_field = models.IntegerField(default=0)
    width_field = models.IntegerField(default=0)
    content = models.TextField()

    draft = models.BooleanField(default=False)
    publish = models.DateField(auto_now=False, auto_now_add=False)

    update = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)  # auto_now - always refresh date

    objects = PostManager()  # you can name variable however (objects - post, my_obj, ) and in views you must to write new name of variable

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("posts:detail", args=[self.id]) #kwargs={"id": self.id})

    class Meta:
        ordering = ["-timestamp", "-update"]


# this function for create another url and we use <slug>
def create_slug(instance, new_slug=None):
    slug = slugify(instance.title)
    if new_slug is not None:
        slug = new_slug
    qs = Post.objects.filter(slug=slug).order_by("-id")
    exists = qs.exists()
    if exists:
        new_slug = "%s-%s" %(slug, qs.first().id)
        return create_slug(instance, new_slug=new_slug)
    return slug

def pre_save_post_reciever(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = create_slug(instance)
    # slug = slugify(instance.title)
    # # if title is 'Some title 2' --> 'Some-title-2' it's for url good
    # exists = Post.objects.filter(slug=slug).exists()
    # if exists:
    #     slug = "%s-%s" %(slug, instance.id)
    # instance.slug = slug

pre_save.connect(pre_save_post_reciever, sender=Post)
